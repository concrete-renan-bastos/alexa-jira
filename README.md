# Alexa–JIRA Interaction

This code uses [alexa-app](https://www.npmjs.com/package/alexa-app) as a
framework to build an Alexa Skill.

To deploy the skill on a staging Echo, we need two services:

1. Create a lambda function via https://console.aws.amazon.com/lambda
2. Create an Alexa skill via https://developer.amazon.com/


## Sample Interaction Phrases

##### StatusOfJiraIssue

```javascript
User : Alexa!  Ask <InvocationWord>, "What is the status of {PROJECTKEY}-{ISSUENUMBER}?"
Alexa: {PROJECTKEY}-{ISSUENUMBER} "{SUMMARY}" has a priority {PRIORITY} and is in status {STATUS}.
```

```javascript
User : Alexa!  Ask JIRA, "What is the status of DEMO-15?"
Alexa: DEMO-15 "Node removed from cluster" has a priority Normal and is in status Done.

User : Alexa!  Ask JIRA, "What is the Status of DEMO-152323?"
Alexa: Sorry, DEMO-152323 does not exists on JIRA server.
```

##### CommentOnJiraIssue

```javascript
User : Alexa!  Ask JIRA, "What is the last comment on {PROJECTKEY}-{ISSUENUMBER}?"
Alexa: The last comment on {PROJECTKEY}-{ISSUENUMBER} is "{COMMENT}".
```

```javascript
User : Alexa!  Ask JIRA, "What is the last comment on DEMO-14?"
Alexa: The last comment on DEMO-14 is "111111111111111".

User : Alexa!  Ask JIRA, "What is the last comment on DEMO-14123?"
Alexa: Sorry, DEMO-14123 does not exists on JIRA server.
```

##### ResolveJiraIssue

```javascript
User : Alexa!  Ask JIRA to resolve {PROJECTKEY}-{ISSUENUMBER}.
Alexa: {PROJECTKEY}-{ISSUENUMBER} has been resolved to Done status.
```

```javascript
User : Alexa!  Ask JIRA to resolve DEMO-13.
Alexa: DEMO-103 has been resolved to Done status.

User : Alexa!  Ask JIRA to resolve DEMO-13111.
Alexa: Sorry, DEMO-13111 does not exists or you don't have permission to modify this issue.
```

##### IssueAssigedToMe

```javascript
User : Alexa!  Ask JIRA, "What is the number of issues assigned to me?"
Alexa: There are no issue assigned to {USER}.
```

##### CreateJIRAIssue

```javascript
User : Alexa!  Ask JIRA, "Create issue in {PROJECTKEY}."
Alexa: New JIRA issue {PROJECTKEY}-{ISSUENUMBER} has been created.
```

```javascript
User : Alexa!  Ask JIRA, "Create issue in DEMO."
Alexa: New JIRA issue DEMO-122 has been created.

User : Alexa!  Ask JIRA, "Create Issue in DEP."
Alexa: Invalid project Key.  Please provide a valid project key to create the issue in.

User : Alexa!  Ask JIRA, "Create issue."
Alexa:  I didn't hear a JIRA project to create the issue in.  Default project that could be used is DEMO.
```

## Setup Alexa Skill for Echo

### Creating a Lambda Function

Set the JIRA details in the `config.json` file

```
JIRAServerURL -  JIRA Server URL
username      -  JIRA username
password      -  JIRA password

Default project to create the issue in and different types of issuetypes
CreateIssueProperties : {
    "ProjectKey" : "DEMO",
    "Issuetypes" : ["Bug","Task","Story"]
}

Transition that resolves an issue
"ResolveJIRAIssue" : {
    "ResolveState" : "Done"
}
```

#### Create Code ZIP

Compress the following files into a zip:

    config.json  
    index.js  
    JIRAend.js  
    package.json  
    README.md

#### Setup Lambda Function

Log into AWS Lambda.  You must be logged in on US-East (N. Virginia) in order to access the Alexa Service from AWS Lambda.  To switch your location, simply click the location displayed next to your name in the top right of the AWS Console.  Click “Create a Lambda Function”.  On the screen about selecting a blueprint, click “skip”.  Now we can configure the Lambda function that will host our skill service by filling out the empty fields here:

1. For Name, enter “jiraService”
2. For Runtime, select “Node.js”
3. For Role, select “`lambda_basic_execution`”

Finally, select “upload a ZIP file” under “Lambda function code” and choose the archive file you created.  You should wind up with a completed form that looks like this:

![Create Lambda Function](Images/FunctionSetup.png "Create Lambda Function")

Click “Next” and then click “Create function”.  This takes us to a screen where we will configure one last detail: the event source.

Click on the “Event source” tab and click “Add event source”.  Select “Alexa Skills Kit” for the Event Source Type and click “Submit”.

![Create Event Source](Images/add-event-source.png "Create Event Source")

Make sure to copy the “ARN” at the top right of the page. This is the Amazon Resource Name, and the skill interface we’re going to configure next needs that value in order to know where to send events.  The ARN will look something like `arn:aws:lambda:us-east-1:333333289684:function:myFunction`.

We can test that the Lambda function works correctly by sending it a test event within the Lambda interface.  Click the blue “Test” button at the right of the screen, and under “Sample Event Template” select “Alexa Start Session”.  Click “Save and Test”.

At this point, the skill service we wrote is live.  Now we can move on to getting the skill interface set up.

### Create Alexa Skill

The AWS Lambda instance is live and we’ve got its ARN value copied, so now we can start configuring the skill interface to do its work: resolving a user’s spoken utterances to events our service can process.  Go to the [Alexa Skills Kit Portal](https://developer.amazon.com/edw/home.html#/skills/list) and click “Add a New Skill”.  Enter “JIRA Service” for “Name” and “JIRA” for “Invocation Name”.

The value for “Invocation Name” is what a user will say to Alexa to trigger the skill.  Paste the ARN you copied into the Endpoint box (Make sure the “Lambda ARN” button is selected).  The Skill Information page should look like this:

![Create Alexa Skill](Images/AlexaSkill1.png "Create Alexa Skill")

#### Setting up the Interaction Model

We’ve already gotten the values needed for the Interaction Model page; it's mentioned below.

Copy the values from the “Schema” and “Utterances” fields on the local server test page and paste them into the respective fields on the “Interaction Model” page

##### Intent list
```javascript
{
  "intents": [
    {
      "intent": "StatusOfJiraIssue",
      "slots": [
        {
          "name": "PROJECTKEY",
          "type": "PKEY"
        },
        {
          "name": "ISSUENUMBER",
          "type": "NUMBER"
        }
      ]
    },
    {
      "intent": "CommentOnJiraIssue",
      "slots": [
        {
          "name": "PROJECTKEY",
          "type": "PKEY"
        },
        {
          "name": "ISSUENUMBER",
          "type": "NUMBER"
        }
      ]
    },
    {
      "intent": "ResolveJiraIssue",
      "slots": [
        {
          "name": "PROJECTKEY",
          "type": "PKEY"
        },
        {
          "name": "ISSUENUMBER",
          "type": "NUMBER"
        }
      ]
    },
    {
      "intent": "IssueAssigedToMe",
      "slots": []
    },
    {
      "intent": "CreateJIRAIssue",
      "slots": [
        {
          "name": "PROJECTKEY",
          "type": "PKEY"
        }
      ]
    }
  ]
}
```

##### Utterances
The utterances recognized by Alexa need to be defined for each Intent.  Here are a list of utterance combinations created for the above Intents; you can add more phrases for each Intent.

```javascript
StatusOfJiraIssue       What is the status {-|PROJECTKEY}-{-|ISSUENUMBER}
StatusOfJiraIssue       Status {-|PROJECTKEY}-{-|ISSUENUMBER}
StatusOfJiraIssue       What is the status of {-|PROJECTKEY}-{-|ISSUENUMBER}
StatusOfJiraIssue       Status of {-|PROJECTKEY}-{-|ISSUENUMBER}
StatusOfJiraIssue       What is the status JIRA issue {-|PROJECTKEY}-{-|ISSUENUMBER}
StatusOfJiraIssue       Status JIRA issue {-|PROJECTKEY}-{-|ISSUENUMBER}
StatusOfJiraIssue       What is the status of JIRA issue {-|PROJECTKEY}-{-|ISSUENUMBER}
StatusOfJiraIssue       Status of JIRA issue {-|PROJECTKEY}-{-|ISSUENUMBER}
CommentOnJiraIssue      last comment {-|PROJECTKEY}-{-|ISSUENUMBER}
CommentOnJiraIssue      What is the last comment {-|PROJECTKEY}-{-|ISSUENUMBER}
CommentOnJiraIssue      last comment on {-|PROJECTKEY}-{-|ISSUENUMBER}
CommentOnJiraIssue      What is the last comment on {-|PROJECTKEY}-{-|ISSUENUMBER}
CommentOnJiraIssue      last comment JIRA issue {-|PROJECTKEY}-{-|ISSUENUMBER}
CommentOnJiraIssue      What is the last comment JIRA issue {-|PROJECTKEY}-{-|ISSUENUMBER}
CommentOnJiraIssue      last comment on JIRA issue {-|PROJECTKEY}-{-|ISSUENUMBER}
CommentOnJiraIssue      What is the last comment on JIRA issue {-|PROJECTKEY}-{-|ISSUENUMBER}
ResolveJiraIssue        Resolve {-|PROJECTKEY}-{-|ISSUENUMBER}
ResolveJiraIssue        Resolve status {-|PROJECTKEY}-{-|ISSUENUMBER}
ResolveJiraIssue        Resolve JIRA issue {-|PROJECTKEY}-{-|ISSUENUMBER}
ResolveJiraIssue        Resolve status JIRA issue {-|PROJECTKEY}-{-|ISSUENUMBER}
IssueAssigedToMe        Issues assigned to me
IssueAssigedToMe        What is the issues assigned to me
IssueAssigedToMe        number of issues assigned to me
IssueAssigedToMe        What is the number of issues assigned to me
IssueAssigedToMe        JIRA issues assigned to me
IssueAssigedToMe        What is the JIRA issues assigned to me
IssueAssigedToMe        number of JIRA issues assigned to me
IssueAssigedToMe        What is the number of JIRA issues assigned to me
CreateJIRAIssue         Create {-|PROJECTKEY}
CreateJIRAIssue         Create JIRA {-|PROJECTKEY}
CreateJIRAIssue         Create issue {-|PROJECTKEY}
CreateJIRAIssue         Create JIRA issue {-|PROJECTKEY}
CreateJIRAIssue         Create in {-|PROJECTKEY}
CreateJIRAIssue         Create JIRA in {-|PROJECTKEY}
CreateJIRAIssue         Create issue in {-|PROJECTKEY}
CreateJIRAIssue         Create JIRA issue in {-|PROJECTKEY}
```

##### Defining the Custom Slot Type

Make sure to add a Custom Slot Type when configuring the Interaction Model in the Alexa Skill

>TYPE - PKEY

>Values - DEMO,TEST <Include the list of Project Keys you would like to interact with. Takes comma-separated values>



### Testing the skills
Use “Sample Utterance” to test the different intents.  **Make sure to log in to Echo with the same account used to create the Alexa Function in the Amazon developer console. Only then will Alexa be able to recognize the Invocation Name for the skill.**


#### References

[Follow the "Setting up AWS Lambda" instructions from the following link](https://www.bignerdranch.com/blog/developing-alexa-skills-locally-with-nodejs-deploying-your-skill-to-staging/)

[Follow the "Configuring the Skill Interface" instructions from the following link](https://www.bignerdranch.com/blog/developing-alexa-skills-locally-with-nodejs-deploying-your-skill-to-staging/)
